define(function () {

	function sayHello(name){
		return name?`Hello ${name}!`:`Hello world!`;
	}
    return {
        sayHello
    };
});