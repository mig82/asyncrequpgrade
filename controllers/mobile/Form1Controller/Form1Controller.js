define(["greeter"], function(greeter){
	return {
		onNavigate: function(){
			this.view.greetLabel.text = greeter.sayHello("Miguel");
		}
	};
});